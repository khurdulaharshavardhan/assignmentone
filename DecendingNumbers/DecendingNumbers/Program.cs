﻿using System;
using System.Collections.Generic;

namespace DecendingNumbers
{
    class SolutionFour
    {
        List<int> numbers = new List<int>();

        static void Main(string[] args)
        {
            SolutionFour solution = new SolutionFour();

            Console.WriteLine("Enter the numbers separated with a comma:");
            string line = Console.ReadLine();

            line = line.Replace(" ", "");
            string[] values = line.Split(",");

            foreach (var charNumber in values)
            {

                solution.numbers.Add(Convert.ToInt32(charNumber));

            }

            //now that we have a completed list..
            //implementing better way of sorting...

            int[] temporaryArray = solution.numbers.ToArray();
            Array.Sort<int>(temporaryArray,
                   new Comparison<int>(
                           (i1, i2) => i2.CompareTo(i1)
                   ));

            //print each element in the array.
            foreach (var number in temporaryArray)
            {
                Console.Write(number + " ");
            }
        }
    }
}
