﻿using System;
using System.Linq;

namespace FindMaximumNumber
{
    class SolutionTwo
    {
        int answer = 0;

        //Method to remove any spaces, that might be present in input string.
        string CleanString(string temp) {
            return  temp.Replace(" ", "");
            
        }

        int FindMax(int[] array) {
            int temp = array[0];

            if (array.Length == 1)
                return temp; //if lenght of the total given numbers is 1, then it is the greatest number itself.
            else {

                temp = array.Max();
                }
            

            return temp;
        }

        static void Main(string[] args)
        {
            SolutionTwo solution = new SolutionTwo();

            Console.WriteLine("Enter sequence of numbers seperated by a comma: ");
            string line = Console.ReadLine();

            string temporaryString = solution.CleanString(line);

            string[] values = temporaryString.Split(",");

            int[] array = new int[values.Length];

            for (int i = 0; i < values.Length; i++){
                array[i] = Convert.ToInt32(values[i]);
            }
            

            solution.answer = solution.FindMax(array);

            Console.Write(solution.answer);
        }
    }
}
