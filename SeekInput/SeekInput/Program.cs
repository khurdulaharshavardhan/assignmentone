﻿using System;

/*Write a program and continuously ask the user to enter a number or "ok" to exit. 
 * Calculate the sum of all the previously entered numbers and display it on the console.*/

namespace SeekInput
{
    class SolutionOne
    {
        public int sum = 0;

        //method to write to console..
        void Prompt() {
            Console.WriteLine("Please enter a number/ok to exit: ");
        }
        static void Main(string[] args)
        {
            SolutionOne solution = new SolutionOne();

            while (true)
            {
                solution.Prompt(); //writes input request to console.
                string givenInput = Console.ReadLine();

                //primary idea is to check if the input is ok each time.
                if (givenInput.ToLower().Equals("ok"))
                {
                    Console.WriteLine("The total value of summation is: {0}", solution.sum);
                    break;
                }
                else {
                    int temporary = Convert.ToInt32(givenInput);
                    solution.sum += temporary;
                }
            }
        }
    }
}
