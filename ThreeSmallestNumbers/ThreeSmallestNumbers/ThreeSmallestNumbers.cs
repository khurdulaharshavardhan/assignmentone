﻿using System;
using System.Collections.Generic;

namespace ThreeSmallestNumbers
{
    class SolutionThree
    {
        List<int> numbers = new List<int>();
        
        static void Main(string[] args)
        {
            SolutionThree obj = new SolutionThree();

            Console.WriteLine("Enter the numbers separated with a comma:");
            string line = Console.ReadLine();

            line = line.Replace(" ","");
            string[] values = line.Split(",");

            foreach (var charNumber in values) {

                obj.numbers.Add(Convert.ToInt32(charNumber));

            }

            if(obj.numbers.Count<5)
                Console.WriteLine("Invalid List!");
            else
            {
                obj.numbers.Sort();
                Console.WriteLine("The three smallest numbers present in the list are: {0}, {1}, and {2}", obj.numbers[0], obj.numbers[1], obj.numbers[2]);
            }
        }
    }
}
